import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';



const routes: Routes = [{
  path: '',
  component: HeaderComponent,
  children: [{
    path: 'login',
    loadChildren: () => import('./pages/login/login.module')
    .then(mod => mod.LoginModule)
  }, {
    path: 'feed',
      loadChildren: () => import('./pages/feed/feed.module')
      .then(mod => mod.FeedModule)
  }, {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
